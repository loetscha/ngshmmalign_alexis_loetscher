#ifndef NGSHMMALIGN_INDEX_IMPL_HPP
#define NGSHMMALIGN_INDEX_IMPL_HPP

/*
 * Copyright (c) 2016 David Seifert
 *
 * This file is part of ngshmmalign
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stack>
#include <string>
#include <iostream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>

namespace
{

// kmer-based lookup
template <typename T>
void reference_genome<T>::create_index(const uint16_t desired_kmer_length)
{
	//m_kmer_length = desired_kmer_length;
	constexpr int32_t max_kmers = 100000000;

	auto expand_ambig_sequence = [&](const uint32_t POS, const uint32_t length) {
		std::stack<char> S;
		char v;
		std::string cur_string;
		uint16_t cur_length = 0;

		for (const auto c : { 'A', 'C', 'G', 'T' })
		{
			if (m_table_of_included_bases[POS][c])
			{
				S.push(c);
			}
		}

		while (!S.empty())
		{
			v = S.top();
			S.pop();

			if (v == 'z')
			{
				// going back in string
				cur_string.pop_back();
				--cur_length;
			}
			else
			{
				cur_string.push_back(v);
				++cur_length;
				S.push('z');

				if (cur_length == length)
				{
					// got a full-length kmer
					auto it = m_kmer_index.find(cur_string);

					if (it == m_kmer_index.end())
					{
						// new kmer not in hash map yet
						m_kmer_index.emplace(cur_string, std::vector<uint32_t>{ POS });
					}
					else
					{
						// kmer already in map
						it->second.push_back(POS);
					}
				}
				else
				{
					// no full length kmer yet
					for (const auto c : { 'A', 'C', 'G', 'T' })
					{
						if (m_table_of_included_bases[POS + cur_length][c])
						{
							S.push(c);
						}
					}
				}
			}
		}
	};

	bool too_large;
	for (m_kmer_length = std::min<decltype(m_kmer_length)>(desired_kmer_length, m_L); m_kmer_length >= 10; --m_kmer_length)
	{
		std::cout << "   Building k-mer index, k = " << m_kmer_length;
		m_kmer_index.clear();

		too_large = false;
		for (std::size_t i = 0; i < m_L - m_kmer_length + 1; ++i)
		{
			expand_ambig_sequence(i, m_kmer_length);

			if (m_kmer_index.size() > max_kmers)
			{
				too_large = true;
				break;
			}
		}

		if (too_large == true)
		{
			std::cout << " -> too large" << std::endl;
		}
		else
		{
			std::cout << " -> " << m_kmer_index.size() << " unique k-mers" << std::endl;
			break;
		}
	}
}

template <typename T>
typename reference_genome<T>::indices reference_genome<T>::find_pos(const boost::string_ref& query) const
{
	boost::accumulators::accumulator_set<
		int64_t, boost::accumulators::features<
		boost::accumulators::tag::mean, boost::accumulators::tag::variance>> acc;
	hash_map_type::const_iterator it;
	std::map<std::int64_t,int> counter; // Kmer counts per region found
	std::map<std::int64_t,int> filtered_counter; // Top n locations
	std::vector<index_stat> possible_index; // Store possible locations here

	uint32_t num_samples = 0;
	uint32_t buffer = 25;
	// minimum number of kmers to consider the location possible
	uint32_t min_n_kmer = 0;
	// number of possible positions
	int top_n = 5;
	
	if (query.length() >= m_kmer_length)
	{
		const std::size_t max_len = query.length() - m_kmer_length + 1;
		for (std::size_t i = 0; i < max_len; ++i)
		{
			boost::string_ref cur_kmer = query.substr(i, m_kmer_length);
			it = m_kmer_index.find(cur_kmer, m_kmer_index.hash_function(), m_kmer_index.key_eq());
			if (it != m_kmer_index.end())
			{
				//std::cerr << cur_kmer << std::endl;
				for (const auto& j : it->second)
				{
					++num_samples;
					int64_t rel_pos = std::max<int64_t>(0, (j-i));
					counter[rel_pos]++;
					//acc(static_cast<int64_t>(j - i));
				}
			}
		}
	}
	
	// Invert counter in order to sort by kmer counts
	std::vector<std::pair<double,size_t>> values;
	for ( auto & v : counter) {
		values.push_back(std::make_pair(v.second,v.first));
    }
    // Sort by kmer counts
	std::vector<size_t> index;
	sortIndex( values, index, false );
	// Keep only top n possible locations
	size_t ic=0;
	for ( auto i : index ) {
		filtered_counter[values[i].second] = values[i].first;
		ic++;
		if (ic==top_n) break;
	}

	//std::cerr << "Pos = "<< acc << std::endl;
	
//	int32_t POS;
//	int32_t sd;
//	if (num_samples)
//	{
//		// add statistics
//		POS = boost::accumulators::mean(acc);
//		sd = std::sqrt(boost::accumulators::variance(acc));
//	}
//	else
//	{
//		// didn't find anything, likely wrong orientation
//		POS = 0;
//		sd = 0;
//	}


	// Count positions with kmers.
	std::vector<int> positions;
	for (auto pair: filtered_counter){
		//std::cerr << pair.first << "," << pair.second << std::endl;
		positions.push_back(pair.first);
	}

	// Aggregate positions if possible and estimate possible genomic ranges
	if (positions.size() > 0){
		for (uint32_t i = 0; i < positions.size(); i++){
			int64_t j = 0;
			uint32_t pos = positions[i];
			uint32_t prev_pos = positions[i-1];
			//TODO: implement filter by number of kmers
			if (num_samples > min_n_kmer){
				// Accumulate if first position visited
				if (i == 0){
					int64_t j = 0;
					do { acc(pos); j++; } while(j <= filtered_counter[pos]);
				} else {
					// Populate positions if the difference in position is larger than read length
					if ((pos-prev_pos) > (query.length() + buffer)){
						index_stat position = {
							boost::accumulators::mean(acc),
							std::sqrt(boost::accumulators::variance(acc)),
							num_samples
						};
						//std::cerr << position.POS << "," <<
						//			position.sd << "," <<
						//			position.num_samples << "," <<
						//			d << std::endl;
						possible_index.push_back(position);
						acc = {};
						do { acc(pos); j++; } while (j <= filtered_counter[pos]);
					} else {
						int64_t j = 0;
						do { acc(pos); j++; } while (j <= filtered_counter[pos]);
					}
				}
			}
		}
		// Populate the last position found
		index_stat position = {
			boost::accumulators::mean(acc),
			std::sqrt(boost::accumulators::variance(acc)),
			num_samples
		};
		possible_index.push_back(position);
	// If no position found, just return some kind of NULL
	} else {
		index_stat position = {0,0,0};
		possible_index.push_back(position);
	}

//    if (positions.size() > 0){
//	    for (int i = 0; i < possible_index.size(); i++ ){
//		    std::cerr << possible_index[i].POS << "," <<
//					    possible_index[i].sd << "," <<
//					    possible_index[i].num_samples << std::endl;
//	    }
//     }

	return possible_index;
//	return { POS, sd, num_samples };
}
}

#endif /* NGSHMMALIGN_INDEX_IMPL_HPP */
